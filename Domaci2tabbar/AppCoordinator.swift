//
//  AppCoordinator.swift
//  Domaci2tabbar
//
//  Created by Luka on 02/05/2019.
//  Copyright © 2019 luka. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    let window: UIWindow
    let tabBar = UITabBarController()
    
    init(window: UIWindow = UIWindow(frame: UIScreen.main.bounds)) {
        self.window = window
    }
    
    func start() {
        setUpTabBarController()
        window.rootViewController = tabBar
        window.makeKeyAndVisible()
    }
    
    func setUpTabBarController(){
        let firstViewController = ViewController()
        firstViewController.view.backgroundColor = UIColor.red
        firstViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        let secondViewController = ViewController()
        secondViewController.view.backgroundColor = UIColor.blue
        secondViewController.tabBarItem = UITabBarItem(tabBarSystemItem: .more, tag: 1)
        let tabBarList = [firstViewController,secondViewController]
        tabBar.viewControllers = tabBarList
    }
}
